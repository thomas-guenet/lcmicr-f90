#@ AJOUT RCMSH Comportement
# coding=utf-8
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# person_in_charge: thomas.guenet at ifsttar.fr

from cata_comportement import LoiComportement

loi = LoiComportement(
    nom            = 'RCMSH',
    doc            = """Rotating Crack Model with Strain Hardening.""",
    num_lc         = 68,
    nb_vari        = 42,
    nom_vari       = ('LOGLOC', 'LOGEOL','LOGALPHA','LOGZERAL','EPSLOC','L0','VIM7', 'PHI0','ALPHA1','ALPHA2','ALPHA3', 'S1','S2','S3','EC1','EC2','EC3','X','Y','Z','AHYPP','EHYPP','XXHYP','E1','E2','E3','W', 'KAPPA1','KAPPA2','KAPPA3','SAPPA1','SAPPA2','SAPPA3','LOGCRAC1','LOGCRAC2','LOGCRAC3','EMAX1','EMAX2','EMAX3','LIMVCLI1','LIMVCLI2','LIMVCLI3'),
    mc_mater       = ('ELAS', 'RCMSH', 'NON_LOCAL'),
    modelisation   = ('3D', 'AXIS', 'CPLAN', 'DPLAN'),
    deformation    = ('PETIT'),
    nom_varc       = ('TEMP', 'SECH', 'HYDR'),
    algo_inte      = ('ANALYTIQUE',),
    type_matr_tang = ('PERTURBATION', 'VERIFICATION', 'TANGENTE_SECANTE'),
    proprietes     = None,
)

