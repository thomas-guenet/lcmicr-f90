! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dGbdtEif(mat, j, eps, tEM, dGbdtEi)
       integer :: j
       real(kind=8) :: eps
        real(kind=8) :: tEM(6)
        real(kind=8) :: dGbdtEi
       real(kind=8) :: mat(22)
   end subroutine dGbdtEif
end interface

