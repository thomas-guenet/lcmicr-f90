! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dot33(m1, m2, sol)
        real(kind=8) :: m1(3,3)
        real(kind=8) :: m2(3,3)
        real(kind=8) :: sol(3,3)
    end subroutine dot33
end interface

