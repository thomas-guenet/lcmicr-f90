! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine tScf(mat, tS, tSc)
        real(kind=8) :: mat(22)
        real(kind=8) :: tS(6)
        real(kind=8) :: tSc(6,6)
    end subroutine tScf
end interface

