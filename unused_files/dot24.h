! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dot24(v, a, w)
        real(kind=8) :: a(6,6)
        real(kind=8) :: v(6)
        real(kind=8) :: w(6)
    end subroutine dot24
end interface

