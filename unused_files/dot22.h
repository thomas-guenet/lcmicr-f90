! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dot22(v1, v2, produit)
        real(kind=8) :: v1(6)
        real(kind=8) :: v2(6)
        real(kind=8) :: produit
    end subroutine dot22
end interface

