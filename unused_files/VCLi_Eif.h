! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine VCLi_Eif(mat, Ei, VCLi_Ei)
        real(kind=8) :: VCLi_Ei
        real(kind=8) :: Ei
        real(kind=8) :: mat(22)
    end subroutine VCLi_Eif
end interface

