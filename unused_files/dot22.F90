subroutine dot22(v1, v2, produit)
! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
    real(kind=8) :: v1(6), v2(6), produit
!
!   Produit scalaire de deux vecteurs (6,1) ou (1,6)
!--------------------------------------------------------------
!   IN
!      v1 : vecteur 
!      v2 : vecteur
!
!   OUT
!      produit : produit scalaire des vecteurs v1 et v2
!
!   VI
!      i : itérateur
!--------------------------------------------------------------
    integer :: i
!
    produit = 0.d0
    do 1 i = 1, 6
      produit = produit + v1(i) * v2(i)
 1  end do
end subroutine dot22

