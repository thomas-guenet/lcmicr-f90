! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    function rtsafeGb(funcGb, e, mat, x1, x2, xacc)
        real(kind=8) :: rtsafeGb
        real(kind=8) :: x1
        real(kind=8) :: x2
        real(kind=8) :: xacc
        real(kind=8) :: mat(22)
        real(kind=8) :: e(3)
        external  funcGb
    end function
end interface
