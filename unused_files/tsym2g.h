! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine tsym2g(A, B)
        real(kind=8) :: A(6)
        real(kind=8) :: B(3,3)
    end subroutine tsym2g
end interface

