subroutine softening_law_tensor_deriv(s, kappa, sappa, logCrack, mat, ecp)
! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "rcmsh_headers/softening_law_deriv.h"
    real(kind=8) :: logCrack(3), s(3), kappa(3), sappa(3), mat(22)
    real(kind=8) :: ecp(3)
!
!   Dérivée de la loi d'évolution de la fissure sous forme de
!   tenseur
!--------------------------------------------------------------
!   IN
!      s     : Tenseur des contraintes propres [MPa]
!      kappa : Tenseur des déformations propres historiques inélastiques max
!      sappa : Tenseur des contraintes propres historiques correspondant à kappa
!      logCrack : Variable interne de l'état de la fissure
!      mat      : Regroupement des données matériaux
!
!   OUT
!      ecp : Tenseur dérivé des déformations inélastiques
!
!   VI
!      i : itérateur
!--------------------------------------------------------------
    integer :: i
!
    ecp(1) = 0.d0
    ecp(2) = 0.d0
    ecp(3) = 0.d0
!
    do 1 i = 1, 3
      call softening_law_deriv(i, s(i), kappa(i), sappa(i), logCrack(i), mat, ecp(i))
 1  end do
end subroutine softening_law_tensor_deriv

