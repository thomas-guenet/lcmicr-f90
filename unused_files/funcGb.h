! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine funcGb(mat, e, epsmi, f, df)
        real(kind=8) :: epsmi
        real(kind=8) :: f
        real(kind=8) :: df
        real(kind=8) :: e(3)
        real(kind=8) :: mat(22)
    end subroutine funcGb
end interface
