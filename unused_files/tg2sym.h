! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine tg2sym(A, B)
        real(kind=8) :: A(3,3)
        real(kind=8) :: B(6)
    end subroutine tg2sym
end interface

