! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine ten4toMat6(A, B)
        real(kind=8) :: A(3,3,3,3)
        real(kind=8) :: B(6,6)
    end subroutine ten4toMat6
end interface

