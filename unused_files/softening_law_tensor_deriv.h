! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine softening_law_tensor_deriv(s, kappa, sappa, logCrack, mat, ecp)
        real(kind=8) :: logCrack(3)
        real(kind=8) :: s(3)
        real(kind=8) :: kappa(3)
        real(kind=8) :: sappa(3)
        real(kind=8) :: mat(22)
        real(kind=8) :: ecp(3)
    end subroutine softening_law_tensor_deriv
end interface

