subroutine tScf(mat, tS, tSc)
! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "asterfort/r8inir.h"
#include "rcmsh_headers/drecif.h"
    real(kind=8) :: mat(22), tS(6), tSc(6,6)
!   Matrix of derive component of softening law
    real(kind=8) :: dreci
    call r8inir(36, 0.d0, tSc, 1)
    call drecif(mat, tS(3), dreci)
    tSc(3,3) = dreci
end subroutine tScf

