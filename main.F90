! MODIF ALGORITH  DATE 11/02/2016   AUTEUR GUENET T.GUENET 
program main
    implicit none
!#include "rcmsh_headers/lcmicr_proj.h"
    character(len=8) :: TYPMOD(2)
    character(len=16) :: COMPOR(6),OPTION
    integer :: IMATE,NDIM,KPG,KSP
!   integer :: ICOMP,NVI
!   integer :: CODRET
!   real(kind=8) :: ANGMAS(2),TAMPON(2)
!   real(kind=8) :: CRIT(2),INSTAM,INSTAP,SIGM(3)
    character(len=2) :: FAMI
    real(kind=8) :: EPSM(6), DEPS(6), VIM(42)
    real(kind=8) :: SIGP(6), VIP(42), DSIDEP(6,6)
!
    integer :: nno, ipoids, idfde, npg
    real(kind=8) :: x, y, z, u, v, w
    real(kind=8) :: Eel, ftel, Eloc, Cloc, epsloc, L0, epsmi
    real(kind=8) :: logloc, logEOL, logAlpha
    real(kind=8) :: alpha(3)
    real(kind=8), dimension(3,4) :: geom !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! imposé nno = 4
    real(kind=8) :: s(3), EPS(6), EPSF(6), ec(3) !,VECPE(3,3)
    real(kind=8) :: logCrack(3), kappa(3), sappa(3), emax(3), limiteVCLi(3)
!   real(kind=8) :: eta, kmi, xie, mat(22), xi, VCLi_xi, ec(3), ecp(3), s(3)
    real(kind=8) :: a_hyp, e_hyp, X_hyp
!
    integer :: etape, logchar1, logchar2, nbpas, etapesup
    integer :: i
!
!-----------------------------
    nbpas = 5000 !80000
    etapesup = 0
    ksp = 1
    kpg = 1
    OPTION = 'RAPH'
    FAMI = 'qu'
    !OPTION = 'RIGI'
!
    do i=1,6 ! initialisation
      EPS(i) = 0.d0
      EPSM(i)= 0.d0
      EPSF(i)= 0.d0
    end do
!
!   do i = 1, 6
!     EPSF(i)  = EPSM(i) + DEPS(i)
!     if (nbpas.GT.1) DEPS(i) = DEPS(i) / nbpas
!   end do
!
    do i = 1, 6 ! initialisation
      EPSM(i) = 0.d0
      DEPS(i) = 0.d0
    end do
!
    DEPS(1) = -0.18d0*0.d0
    DEPS(2) = -0.18d0*0.d0
    DEPS(3) = 1.d0
    DEPS(4) = 0.d0
    DEPS(5) = 0.d0
    DEPS(6) = 0.d0
    do i = 1, 6
      DEPS(i)  = 1.d-6*DEPS(i)
    end do
!
    nno       = 4
    logloc    = 0.d0
    logEOL    = 0.d0
    logalpha  = 0.d0
    L0        = 0.d0
    epsmi     = 0.d0
    alpha(1)  = 0.d0
    alpha(2)  = 0.d0  
    alpha(3)  = 0.d0   
    s(1)      = 0.d0
    s(2)      = 0.d0
    s(3)      = 0.d0
    Eel  = 0.d0
    ftel = 0.d0
    Eloc = 0.d0
    Cloc = 0.d0
    epsloc = 0.d0
    a_hyp = 0.d0
    e_hyp = 0.d0
    X_hyp = 0.d0
    u = 0.d0
    v = 0.d0
    w = 0.d0
    x = 0.d0
    y = 0.d0
    z = 0.d0
    do i = 1, 3
      kappa(i)      = 0.d0
      sappa(i)      = 0.d0
      logCrack(i)   = 0.d0
      emax(i)       = 0.d0
      limiteVCLi(i) = 0.d0
      ec(i)         = 0.d0
    end do
!
    VIM(1)  = logloc
    VIM(2)  = logEOL
    VIM(3)  = logAlpha
    VIM(4)  = 0.d0 ! inutilisé
    VIM(5)  = epsloc
    VIM(6)  = L0
    VIM(7)  = epsmi
    VIM(8)  = 0.d0 ! inutilisé
    VIM(9)  = alpha(1)
    VIM(10) = alpha(2)
    VIM(11) = alpha(3)
    VIM(12) = s(1)
    VIM(13) = s(2)
    VIM(14) = s(3)
    VIM(15) = ec(1)
    VIM(16) = ec(2)
    VIM(17) = ec(3)
    VIM(18) = x !
    VIM(19) = y !
    VIM(20) = z !
    VIM(21) = a_hyp
    VIM(22) = e_hyp
    VIM(23) = X_hyp
    VIM(24) = 0.d0!e(1)
    VIM(25) = 0.d0!e(2)
    VIM(26) = 0.d0!e(3)
    VIM(27) = 0.d0
    VIM(28) = kappa(1)
    VIM(29) = kappa(2)
    VIM(30) = kappa(3)
    VIM(31) = sappa(1)
    VIM(32) = sappa(2)
    VIM(33) = sappa(3)
    VIM(34) = logCrack(1)
    VIM(35) = logCrack(2)
    VIM(36) = logCrack(3)
    VIM(37) = emax(1)
    VIM(38) = emax(2)
    VIM(39) = emax(3)
    VIM(40) = limiteVCLi(1)
    VIM(41) = limiteVCLi(2)
    VIM(42) = limiteVCLi(3)
       
! CORPS DU PROGRAMME
!   call tChomf(Em,nu,eps0,0.02d0,tChom) ! ok
!   call tChomInvf(Em,nu,eps0,0.02d0,tChom) ! ok
!   call dtChomdepsf(Em,nu,eps0,0.02d0,tChom) ! ok
!   call dtChomddepsf(Em,nu,eps0,0.02d0,tChom) ! ok
!
!   call dwdaf(mat,1.2d0,EPS,dWda) ! ok
!   call ddWddaf(mat,1.2d0,EPS,ddWdda) ! ok
!   call Gbepsf(mat,0.02d0,EPS,Gbeps) ! ok
!   call Gaepsf(mat,0.02d0,EPS,Gaeps) ! ok
!   call Repsf(mat,0.02d0,Reps) ! ok
!   call loading_func(mat,0.02d0,EPS,Reps) ! ok
!   call dGbdepsf(mat,0.02d0,EPS,Reps) ! ok
!
!   call fiber_extraction_inv(mat,0.02d0,Reps) ! ok
!   call fiber_extraction_law(mat,xi,Reps) ! ok
!   call VCLi_Eif(mat,xi,Reps) ! ok
!   call fiber_extraction_inv_deriv(mat,1.32d0,Reps) ! ok
!
    ndim = 3
!
    OPEN(UNIT=1,&
      FILE='eps_sig_I.txt',&
      POSITION='rewind',&
      STATUS='unknown')
    OPEN(UNIT=2,&
      FILE='eps_sig_II.txt',&
      POSITION='rewind',&
      STATUS='unknown')
    OPEN(UNIT=3,&
      FILE='eps_sig_III.txt',&
      POSITION='rewind',&
      STATUS='unknown')
!
!   WRITE (UNIT=1,FMT='(F20.7,A1,F20.7)') EPS(1),',',0.d0
!   WRITE (UNIT=2,FMT='(F20.7,A1,F20.7)') EPS(2),',',0.d0
!   WRITE (UNIT=3,FMT='(F20.7,A1,F20.7)') EPS(3),',',0.d0
!
    logchar1 = 1
    logchar2 = 1
!
!###########################################################################"
    do etape = 1,nbpas
!
      call lcmicr(fami, kpg, ksp, ndim, typmod,&
                  imate, compor, epsm, deps, vim,&
                  option, sigp, vip, dsidep,&
                  nno, geom, ipoids, idfde, npg)
!
      do i = 1, 6
        EPS(i) = EPSM(i) + DEPS(i)
      end do
!
     !WRITE (UNIT=2,FMT='(F14.7,A1,F14.7)') EPS(3),',',SIGP(3)
     !WRITE (UNIT=1,FMT='(I5,A1,F20.7)') etape,',',VIP(17) !!! rouge
     !WRITE (UNIT=2,FMT='(F20.7,A1,F20.7)') VIP(25),',',VIP(13)
     WRITE (UNIT=3,FMT='(F20.7,A1,F20.7)') VIP(26),',',VIP(14) !!!!!!! le vert
     !WRITE (UNIT=3,FMT='(I5,A1,F20.7)') etape,',',VIP(26)
!
      if ((etape.gt.7000).AND.(logchar1.eq.1)) then ! dechargement
        do i = 1, 6
          DEPS(i) = -DEPS(i)
        end do
        print*,'déchargement',' logLOC=',VIP(1)
        logchar1 = 0
      end if
!
      if ((etape.gt.14100).AND.(logchar2.eq.1)) then ! rechargement
        do i = 1, 6
          DEPS(i) = -DEPS(i)
        end do
        print*,'rechargement',' logLOC=',VIP(1)
        logchar2 = 0
      end if
!
      do i = 1, 42
        VIM(i) = VIP(i)
      end do
      do i = 1, 6
        EPSM(i) = EPS(i)
      end do
!
    end do
!
    CLOSE (UNIT=1)
    CLOSE (UNIT=2)
    CLOSE (UNIT=3)
!
    print*,'epsloc  : ',VIP(19)
    print*,'SIGP    : ',SIGP !VIP(12),VIP(13),VIP(14)
    print*,'epsmi   : ',VIP(7)
    print*,'logCrack: ',VIP(34),VIP(35),VIP(36)
    print*,'emaxi   : ',VIP(37),VIP(38),VIP(39)
!   print*,'DSIDEP  : '
!   print*, DSIDEP
!
end program main
