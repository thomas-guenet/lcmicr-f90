from time              import clock
from math              import sqrt#,pi,cos,sin,cosh,exp,acos
from scipy             import dot
import numpy               as np
import matplotlib.pyplot   as plt

if 0:
    fortran1 = open('eps_sig_I.txt','r')
    fort = fortran1.readlines()
    fortran1.close()
    AX = np.zeros(len(fort))
    AY = np.zeros(len(fort))
    i = 0
    for fo in fort :
        A  = fo.split(',')
        AX[i] = float(A[0])
        AY[i] = float(A[1])
        i += 1

if 0:
    fortran2 = open('eps_sig_II.txt','r')
    fort = fortran2.readlines()
    fortran2.close()
    BX = np.zeros(len(fort))
    BY = np.zeros(len(fort))
    i = 0
    for fo in fort :
        B  = fo.split(',')
        BX[i] = float(B[0])
        BY[i] = float(B[1])
        i += 1

if 1:
    fortran3 = open('eps_sig_III.txt','r')
    fort = fortran3.readlines()
    fortran3.close()
    CX = np.zeros(len(fort))
    CY = np.zeros(len(fort))
    i = 0
    for fo in fort :
        C  = fo.split(',')
        CX[i] = float(C[0])
        CY[i] = float(C[1])
        i += 1


if  1:
    fig = plt.figure(1)
    plt.plot(CX,CY,'g.-',lw=1,label='tepo3')
    #plt.plot(AX,AY,'r.-',lw=1,label='tepo1')
    #plt.plot(BX,BY,'b.-',lw=1,label="tepo2")
    plt.xlabel(r'$E_z$ [-]'       ,fontsize=24)
    plt.ylabel(r'$\Sigma_z$ [MPa]',fontsize=24)
    plt.grid(True)


plt.show()
