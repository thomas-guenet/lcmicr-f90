! MODIF ALGORITH  DATE 19/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dqelg(n,epstab,result,abserr,res3la,nres)
        integer :: n
        real(kind=8) :: epstab(52)
        real(kind=8) :: result
        real(kind=8) :: abserr
        real(kind=8) :: res3la(3)
        integer :: nres
    end subroutine
end interface
