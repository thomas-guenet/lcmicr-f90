! MODIF ALGORITH  DATE 28/08/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dqk21hn(a,b,c,d,result,abserr,resabs,resasc)
        real(kind=8) :: a
        real(kind=8) :: b
        real(kind=8) :: c
        real(kind=8) :: d
        real(kind=8) :: result
        real(kind=8) :: abserr
        real(kind=8) :: resabs
        real(kind=8) :: resasc
    end subroutine
end interface
