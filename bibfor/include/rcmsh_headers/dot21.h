! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dot21(a, v, w)
        real(kind=8) :: a(3,3)
        real(kind=8) :: v(3)
        real(kind=8) :: w(3)
    end subroutine dot21
end interface

