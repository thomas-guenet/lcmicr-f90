! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine Gbepsf(mat, eps, e, Gbeps)
        real(kind=8) :: mat(13)
        real(kind=8) :: eps
        real(kind=8) :: e(3)
        real(kind=8) :: Gbeps
    end subroutine Gbepsf
end interface

