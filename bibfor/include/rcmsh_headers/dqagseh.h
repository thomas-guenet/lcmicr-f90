! MODIF ALGORITH  DATE 26/08/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dqagseh(a,b,c,d,epsabs,epsrel,limit,result,abserr,neval,ier)
        real(kind=8) :: a
        real(kind=8) :: b
        real(kind=8) :: c
        real(kind=8) :: d
        real(kind=8) :: epsabs
        real(kind=8) :: epsrel
        integer :: limit
        real(kind=8) :: result
        real(kind=8) :: abserr
        integer :: neval
        integer :: ier
    end subroutine
end interface
