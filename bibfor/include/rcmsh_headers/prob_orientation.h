! MODIF ALGORITH  DATE 06/01/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine prob_orientation(phi, theta, prob)
        real(kind=8) :: phi
        real(kind=8) :: theta
        real(kind=8) :: prob
    end subroutine prob_orientation
end interface

