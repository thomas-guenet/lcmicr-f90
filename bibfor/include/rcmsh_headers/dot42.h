! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dot42(a, v, w)
        real(kind=8) :: a(6,6)
        real(kind=8) :: v(6)
        real(kind=8) :: w(6)
    end subroutine dot42
end interface

