! MODIF ALGORITH  DATE 19/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine rtsafe(e, mat, x1, x2, xacc, rtsafed)
        real(kind=8) :: x1
        real(kind=8) :: x2
        real(kind=8) :: xacc
        real(kind=8) :: mat(13)
        real(kind=8) :: e(3)
        real(kind=8) :: rtsafed
    end subroutine rtsafe
end interface

