! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine Gaepsf(mat, eps, e, Gaeps)
        real(kind=8) :: mat(13)
        real(kind=8) :: eps
        real(kind=8) :: e(3)
        real(kind=8) :: Gaeps
    end subroutine Gaepsf
end interface

