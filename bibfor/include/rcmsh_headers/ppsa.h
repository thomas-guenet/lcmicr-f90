! MODIF ALGORITH  DATE 17/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine projectionPointSurAxe(P, u, A, H)
        real(kind=8) :: P(3)
        real(kind=8) :: u(3)
        real(kind=8) :: A(3)
        real(kind=8) :: H(3)
    end subroutine projectionPointSurAxe
end interface

