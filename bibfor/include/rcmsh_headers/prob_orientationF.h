! MODIF ALGORITH  DATE 26/08/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine prob_orientationF(theta, prob, c, d)
        real(kind=8) :: theta
        real(kind=8) :: prob
        real(kind=8) :: c
        real(kind=8) :: d
    end subroutine prob_orientationF
end interface

