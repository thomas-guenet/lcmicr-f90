! MODIF ALGORITH  DATE 20/05/2015  AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine lcmicr(fami, kpg, ksp, ndim, typmod,&
                  imate, compor, epsm, deps, vim,&
                  option, sig, vip, dsidep, nno, geom, ipoids, idfde, npg)
        character(len=8) :: typmod(2)
        character(len=16) :: compor(*)
        character(len=16) :: option
        character(len=*) :: fami
        integer :: ndim
        integer :: imate
        integer :: kpg
        integer :: ksp
        real(kind=8) :: epsm(6)
        real(kind=8) :: deps(6)
        real(kind=8) :: vim(42)
        real(kind=8) :: sig(6)
        real(kind=8) :: vip(42)
        real(kind=8) :: dsidep(6, 6)
        integer :: nno
        real(kind=8) :: geom(3, nno)
        integer :: ipoids
        integer :: idfde
        integer :: npg
    end subroutine
end interface
