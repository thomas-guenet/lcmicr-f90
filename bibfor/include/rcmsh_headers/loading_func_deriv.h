! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine loading_func_deriv(mat, e, epsmi, f, df)
        real(kind=8) :: epsmi
        real(kind=8) :: f
        real(kind=8) :: df
        real(kind=8) :: e(3)
        real(kind=8) :: mat(13)
    end subroutine loading_func_deriv
end interface

