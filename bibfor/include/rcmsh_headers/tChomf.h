! MODIF ALGORITH  DATE 17/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine tChomf(Em, nu, eps0, epsp, tChom)
        real(kind=8) :: Em
        real(kind=8) :: nu
        real(kind=8) :: eps0
        real(kind=8) :: epsp
        real(kind=8) :: tChom(6,6)
    end subroutine tChomf
end interface

