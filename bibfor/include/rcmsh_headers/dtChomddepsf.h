! MODIF ALGORITH  DATE 12/10/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dtChomddepsf(Em, nu, eps0, epsp, dtChomddeps)
        real(kind=8) :: Em
        real(kind=8) :: nu
        real(kind=8) :: eps0
        real(kind=8) :: epsp
        real(kind=8) :: dtChomddeps(6,6)
    end subroutine dtChomddepsf
end interface
