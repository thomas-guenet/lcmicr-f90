! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine softening_law_deriv(axe, si, kappai, sappai, lci, mat, ecpi)
        integer :: axe
        real(kind=8) :: lci
        real(kind=8) :: si
        real(kind=8) :: kappai
        real(kind=8) :: sappai
        real(kind=8) :: mat(13)
        real(kind=8) :: ecpi
    end subroutine softening_law_deriv
end interface

