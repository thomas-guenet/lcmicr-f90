! MODIF ALGORITH  DATE 28/08/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine prob_orientationFn(theta, prob, c, d)
        real(kind=8) :: theta
        real(kind=8) :: prob
        real(kind=8) :: c
        real(kind=8) :: d
    end subroutine prob_orientationFn
end interface

