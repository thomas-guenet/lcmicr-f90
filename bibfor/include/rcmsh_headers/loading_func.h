! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine loading_func(mat, eps, e, f)
        real(kind=8) :: mat(13)
        real(kind=8) :: eps
        real(kind=8) :: e(3)
        real(kind=8) :: f
    end subroutine loading_func
end interface

