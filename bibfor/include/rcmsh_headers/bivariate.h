! MODIF ALGORITH  DATE 05/08/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine bivariate(x, y, p)
        real(kind=8) :: x, y
        real(kind=8) :: p
    end subroutine bivariate
end interface
