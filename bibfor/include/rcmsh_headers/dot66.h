! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dot66(m1, m2, sol)
        real(kind=8) :: m1(6,6)
        real(kind=8) :: m2(6,6)
        real(kind=8) :: sol(6,6)
    end subroutine dot66
end interface

