! MODIF ALGORITH  DATE 19/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dqpsrt(limit,last,maxerr,ermax,elist,iord,nrmax)
        real(kind=8), dimension(last) :: elist
        real(kind=8) :: ermax
        integer, dimension(last) :: iord
        integer :: last
        integer :: limit
        integer :: maxerr
        integer :: nrmax
    end subroutine
end interface
