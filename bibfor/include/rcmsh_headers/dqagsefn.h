! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dqagsefn(a,b,theta_cst,epsabs,epsrel,limit,result,abserr,neval,ier)
        real(kind=8) :: a
        real(kind=8) :: b
        real(kind=8) :: theta_cst
        real(kind=8) :: epsabs
        real(kind=8) :: epsrel
        integer :: limit
        real(kind=8) :: result
        real(kind=8) :: abserr
        integer :: neval
        integer :: ier
    end subroutine
end interface
