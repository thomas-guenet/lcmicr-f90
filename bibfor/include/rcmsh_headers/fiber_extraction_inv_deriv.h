! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine fiber_extraction_inv_deriv(axe, mat, SigB, deci)
        integer :: axe
        real(kind=8) :: mat(13)
        real(kind=8) :: SigB
        real(kind=8) :: deci
    end subroutine fiber_extraction_inv_deriv
end interface

