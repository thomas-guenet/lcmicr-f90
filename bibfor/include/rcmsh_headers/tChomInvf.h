! MODIF ALGORITH  DATE 17/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine tChomInvf(Em, nu, eps0, epsp, tSe)
        real(kind=8) :: Em
        real(kind=8) :: nu
        real(kind=8) :: eps0
        real(kind=8) :: epsp
        real(kind=8) :: tSe(6,6)
    end subroutine tChomInvf
end interface
