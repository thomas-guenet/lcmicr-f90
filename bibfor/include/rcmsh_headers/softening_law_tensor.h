! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine softening_law_tensor(s, kappa, sappa, logCrack, mat, ec)
        real(kind=8) :: logCrack(3)
        real(kind=8) :: s(3)
        real(kind=8) :: kappa(3)
        real(kind=8) :: sappa(3)
        real(kind=8) :: mat(13)
        real(kind=8) :: ec(3)
    end subroutine softening_law_tensor
end interface

