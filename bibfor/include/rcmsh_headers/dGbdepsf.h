! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dGbdepsf(mat, eps, e, dGbdeps)
        real(kind=8) :: mat(13)
        real(kind=8) :: eps
        real(kind=8) :: e(3)
        real(kind=8) :: dGbdeps
    end subroutine dGbdepsf
end interface

