! MODIF ALGORITH  DATE 12/10/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dRdepsf(mat, eps, dReps)
        real(kind=8) :: mat(13)
        real(kind=8) :: eps
        real(kind=8) :: dReps
    end subroutine dRdepsf
end interface

