! MODIF ALGORITH  DATE 17/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine distMaxPointsElement(nno, element, dmax)
        integer :: nno
        real(kind=8) :: element(3,nno)
        real(kind=8) :: dmax
    end subroutine distMaxPointsElement
end interface

