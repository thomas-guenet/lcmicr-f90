! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine fiber_extraction_law(axe, mat, xi, VCLi_xi)
        integer :: axe
        real(kind=8) :: mat(13)
        real(kind=8) :: xi
        real(kind=8) :: VCLi_xi
    end subroutine fiber_extraction_law
end interface

