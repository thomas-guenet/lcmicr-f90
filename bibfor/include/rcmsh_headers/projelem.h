! MODIF ALGORITH  DATE 17/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine projectionElement(nno, element, u, elementP)
        integer :: nno
        real(kind=8) :: element(3,nno)
        real(kind=8) :: u(3)
        real(kind=8) :: elementP(3,nno)
    end subroutine projectionElement
end interface

