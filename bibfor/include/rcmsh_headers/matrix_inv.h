! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine matrix_inv(matrix, inverse, n, errorflag)
      integer :: n
      integer :: errorflag  
      real(kind=8), dimension(n,n) :: matrix  
      real(kind=8), dimension(n,n) :: inverse 
    end subroutine matrix_inv
end interface
