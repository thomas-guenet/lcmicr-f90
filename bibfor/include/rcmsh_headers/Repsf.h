! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine Repsf(mat, eps, Reps)
        real(kind=8) :: mat(13)
        real(kind=8) :: eps
        real(kind=8) :: Reps
    end subroutine Repsf
end interface

