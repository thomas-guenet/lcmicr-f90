! MODIF ALGORITH  DATE 12/10/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dGadepsf(mat, eps, e, dGaeps)
        real(kind=8) :: mat(13)
        real(kind=8) :: eps
        real(kind=8) :: e(3)
        real(kind=8) :: dGaeps
    end subroutine dGadepsf
end interface

