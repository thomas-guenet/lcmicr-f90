! MODIF ALGORITH  DATE 19/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dqk21f(a,b,theta_cst,result,abserr,resabs,resasc)
        real(kind=8) :: a
        real(kind=8) :: b
        real(kind=8) :: theta_cst
        real(kind=8) :: result
        real(kind=8) :: abserr
        real(kind=8) :: resabs
        real(kind=8) :: resasc
    end subroutine
end interface
