! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine fiber_extraction_inv(axe, mat, SigB, eci)
        integer :: axe
        real(kind=8) :: eci
        real(kind=8) :: SigB
        real(kind=8) :: mat(13)
    end subroutine fiber_extraction_inv
end interface

