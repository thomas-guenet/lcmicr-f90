! MODIF ALGORITH  DATE 28/08/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine bivariate_norme(x, y, p)
        real(kind=8) :: x, y
        real(kind=8) :: p
    end subroutine bivariate_norme
end interface
