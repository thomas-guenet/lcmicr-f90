! MODIF ALGORITH  DATE 20/11/2014   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine softening_law(axe, si, kappai, sappai, lci, mat, eci)
        integer :: axe
        real(kind=8) :: lci
        real(kind=8) :: si
        real(kind=8) :: kappai
        real(kind=8) :: sappai
        real(kind=8) :: mat(13)
        real(kind=8) :: eci
    end subroutine softening_law
end interface

