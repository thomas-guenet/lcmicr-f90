! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine dWdaf(mat, a, e, dWda)
        real(kind=8) :: mat(13)
        real(kind=8) :: a
        real(kind=8) :: e(3)
        real(kind=8) :: dWda
    end subroutine dWdaf
end interface

