! MODIF ALGORITH  DATE 20/05/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
interface
    subroutine lc0068tg(fami, kpg, ksp, ndim, imate,& ! Ajout T.Guenet
                        compor, crit, instam, instap, epsm,&
                        deps, sigm, vim, option, angmas,&
                        sigp, vip, tampon, typmod, icomp,&
                        nvi, dsidep, codret, nno, geom, ipoids, idfde, npg) ! Ajout T.Guenet
        character(len=*) :: fami
        integer :: kpg
        integer :: ksp
        integer :: ndim
        integer :: imate
        character(len=16) :: compor(*)
        real(kind=8) :: crit(*)
        real(kind=8) :: instam
        real(kind=8) :: instap
        real(kind=8) :: epsm(6)
        real(kind=8) :: deps(6)
        real(kind=8) :: sigm(6)
        real(kind=8) :: vim(*)
        character(len=16) :: option
        real(kind=8) :: angmas(3)
        real(kind=8) :: sigp(6)
        real(kind=8) :: vip(*)
        real(kind=8) :: tampon(*)
        character(len=8) :: typmod(*)
        integer :: icomp
        integer :: nvi
        real(kind=8) :: dsidep(6, 6)
        integer :: codret
        integer :: nno ! Ajout T.Guenet
        real(kind=8) :: geom(3,nno) ! Ajout T.Guenet
        integer :: ipoids ! Ajout T.Guenet
        integer :: idfde ! Ajout T.Guenet
        integer :: npg ! Ajout T.Guenet
    end subroutine lc0068tg
end interface
