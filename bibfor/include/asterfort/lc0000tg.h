! MODIF ALGORITH  DATE 20/05/2015   AUTEUR GUENET T.GUENET
!
!###############################################################
!
#include "asterf_types.h"
!
interface
    subroutine lc0000tg(fami, kpg, ksp, ndim, typmod,& ! Ajout T.Guenet
                        imate, compor, crit, instam, instap,&
                        neps, epsm, deps, nsig, sigm,&
                        vim, option, angmas, nwkin, wkin,&
                        cp, numlc, tempd, tempf, tref,&
                        sigp, vip, ndsde, dsidep, icomp,&
                        nvi, nwkout, wkout, codret,&
                        nno, geom, ipoids, idfde, npg) ! Ajout T.Guenet
        integer :: nwkout
        integer :: nvi
        integer :: ndsde
        integer :: nwkin
        integer :: nsig
        integer :: neps
        character(len=*) :: fami
        integer :: kpg
        integer :: ksp
        integer :: ndim
        character(len=8) :: typmod(*)
        integer :: imate
        character(len=16) :: compor(*)
        real(kind=8) :: crit(*)
        real(kind=8) :: instam
        real(kind=8) :: instap
        real(kind=8) :: epsm(neps)
        real(kind=8) :: deps(neps)
        real(kind=8) :: sigm(nsig)
        real(kind=8) :: vim(nvi)
        character(len=16) :: option
        real(kind=8) :: angmas(3)
        real(kind=8) :: wkin(nwkin)
        aster_logical :: cp
        integer :: numlc
        real(kind=8) :: tempd
        real(kind=8) :: tempf
        real(kind=8) :: tref
        real(kind=8) :: sigp(nsig)
        real(kind=8) :: vip(nvi)
        real(kind=8) :: dsidep(ndsde)
        integer :: icomp
        real(kind=8) :: wkout(nwkout)
        integer :: codret
        integer :: nno ! Ajout T.Guenet
        real(kind=8) :: geom(3,nno) ! Ajout T.Guenet
        integer :: ipoids ! Ajout T.Guenet
        integer :: idfde ! Ajout T.Guenet
        integer :: npg ! Ajout T.Guenet
    end subroutine lc0000tg
end interface

