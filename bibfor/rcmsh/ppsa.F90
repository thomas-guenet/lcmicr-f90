subroutine projectionPointSurAxe(P, u, A, H)
! MODIF ALGORITH  DATE 17/11/2014   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
    real(kind=8) :: P(3), u(3), A(3), H(3)
!
!   Donne les coordonées du projeté du point P sur l'axe de vecteur directeur u passant par A.
!--------------------------------------------------------------
!   IN
!      P : Coordonnées du point à projeté sur l'axe
!      u : Vecteur directeur de l'axe
!      A : Coordonnées d'un point de l'axe
!
!   OUT
!      H : Coordonnées de la projection
!
!   VI
!      d : réel issu de l'équation du plan : ax + by + cz + d = 0
!      t : réel issu de l'équation d'une droite : AH = t u
!--------------------------------------------------------------
    real(kind=8) :: d, t
!
!   L'équation cartésienne du plan de normal u passant par P est 
!   ax + by + cz + d = 0 ; (a, b, c) = u ; 
!   d est obtenu en calculant un point du plan d = -np.dot(u,P)
    d = - u(1)*P(1) - u(2)*P(2) - u(3)*P(3)

!   On cherche H le projeté de P sur l'axe : 4 eq, 4 inc
!        ax + by + cz  = -d (1 eq)
!        AH = t u           (3 eq) où t est un scalaire relatif
    t = - ( u(1)*A(1) + u(2)*A(2) + u(3)*A(3) + d ) /&
                   ( u(1)**2 + u(2)**2 + u(3)**2 )
    H(1) = A(1) + u(1) * t
    H(2) = A(2) + u(2) * t
    H(3) = A(3) + u(3) * t
end subroutine projectionPointSurAxe

