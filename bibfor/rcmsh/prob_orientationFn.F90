subroutine prob_orientationFn(theta, prob, c, d)
! MODIF ALGORITH  DATE 06/01/2015   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "rcmsh_headers/dqagsefn.h"
    real(kind=8) :: theta, prob, c, d
!
!   Fonction intégrale de phi1 à phi2 de prob_orientation
!--------------------------------------------------------------
!   IN
!      theta : 
!      c : borne inférieure
!      d : borne supérieure
!
!   OUT
!      prob : 
!--------------------------------------------------------------
    integer :: neval, ier, limit
    real(kind=8) :: epsabs, epsrel, abserr
    real(kind=8) :: theta_cst
!
    theta_cst = theta
    prob   = 0.d0
    epsabs = 1.d-9
    epsrel = 1.d-9
    limit  = 1
!
!   Integration de la fonction prob_orientation(y,x_constant)
    call dqagsefn(c, d, theta_cst, epsabs, epsrel, limit, prob,&
                 abserr, neval, ier)
!
end subroutine prob_orientationFn

