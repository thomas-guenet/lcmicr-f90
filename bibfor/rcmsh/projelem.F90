subroutine projectionElement(nno, element, u, elementP)
! MODIF ALGORITH  DATE 17/11/2014   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "rcmsh_headers/ppsa.h"
#include "asterfort/r8inir.h"
    integer :: nno
    real(kind=8) :: element(3,nno), elementP(3,nno), u(3)
!
!   Projection de l'élément sur l'axe de vecteur directeur u
!--------------------------------------------------------------
!   IN
!      nno      : nombre de noeuds de l'élément
!      element  : coordonnées des points de l'élément à projeter
!      u        : vecteur directeur de l'axe
!
!   OUT
!      elementP : coordonnées des projetés des points de l'élément
!
!   VI
!      nbComposant : nombre de réels nécessaire pour définir l'éléments
!      i   : itérateur
!      A   : Coordonnées du 1er  noeud de l'élément (3)
!      Pi  : Coordonnées du ieme noeud de l'élément (3)
!      PPi : Coordonnées du ieme noeud projeté sur l'axe (3)
!--------------------------------------------------------------
    integer :: i, nbComposant
    real(kind=8) :: A(3), Pi(3), PPi(3)
!
    nbComposant = 3 * nno
    call r8inir(nbComposant, 0.d0, elementP, 1) ! np.zeros((nbNoeuds,3))  # initialisation de l'élément projeté sur l'axe 3 = 3D
    elementP(1,1) = element(1,1)                ! impose que l'axe passe par le premier noeud de l'élément
    elementP(2,1) = element(2,1)
    elementP(3,1) = element(3,1)
    A(1) = element(1,1)
    A(2) = element(2,1)
    A(3) = element(3,1)
!
    do 1 i = 2, nno
       Pi(1) = element(1,i)
       Pi(2) = element(2,i)
       Pi(3) = element(3,i)
       call projectionPointSurAxe(Pi, u, A, PPi)
       elementP(1,i) = PPi(1)            ! impose que l'axe passe par le premier noeud de l'élément
       elementP(2,i) = PPi(2)
       elementP(3,i) = PPi(3)
 1  end do
end subroutine projectionElement

