subroutine dGbdepsf(mat, eps, e, dGbdeps)
! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "rcmsh_headers/dWdaf.h"
#include "rcmsh_headers/ddWddaf.h"
    real(kind=8) :: mat(13), eps, e(3), dGbdeps
!
!   Calcul la dérivée du taux de dissipation d'énergie des fibres
!--------------------------------------------------------------
!   IN
!      mat : Regroupement des données matériaux
!      eps : Endommagement micromécanique
!      e   : Tenseur des déformations propres (3)
!
!   OUT
!      dGbdeps : Dérivée du taux de dissipation d'énergie par rapport à eps
!
!   VI
!      a   : Demi-grand axe de l'éllipsoide (fissure) [mm]
!      Nc  : Nombre de fissures dans le volume élémentaire représentatif [/mm3]
!      dW  : Quantité d'énergie dissipée pour une fissure [N/mm]
!      ddW : Dérivée de la quantité d'énergie dissipée pour une fissure par rapport à a [N/mm2]
!      dadeps : Dérivée de a par rapport à eps
!      ddaddeps : Dérivée seconde de a par rapport à eps
!--------------------------------------------------------------
    real(kind=8) :: a, Nc, dW, ddW, dadeps, ddaddeps
!
    Nc = mat(10)
    a  = (eps/Nc)**(1.d0/3.d0)
    call dWdaf(mat, a, e, dW)
    call ddWddaf(mat, a, e, ddW)
    dadeps   = 1.d0/(3.d0*(eps/Nc)**(2.d0/3.d0)*Nc)
    ddaddeps = -2.d0/(9.d0*(eps/Nc)**(5.d0/3.d0)*Nc**2.d0)
    dGbdeps  = Nc*( ddaddeps * dW + dadeps**2.d0 * ddW )
end subroutine dGbdepsf

