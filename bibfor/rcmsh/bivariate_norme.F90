subroutine bivariate_norme(x, y, p)
! MODIF ALGORITH  DATE 28/08/2015   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
    real(kind=8) :: x, y, p
!
!   Norme de la loi normale à deux variables ou multinormale (pdf) dans le repère AzEl 
!--------------------------------------------------------------
!   IN
!      x, y : coordonnée du point dont on veut la probabilité
!
!   OUT
!      p : densité de probabilité de la loi normale à deux variables
!
!   VI
!      vi1 :
!      mux, muy : coordonnée du centre de la loi normale à deux variables
!      sigmax, sigmay : écart-types de la loi normale à deux variables
!      sigmaxy : terme de covariance
!--------------------------------------------------------------
    real(kind=8) :: Xmu, Ymu, rho, z, pi, pi2, denom
    real(kind=8) :: sigmax, sigmay, sigmaxy
    real(kind=8) :: xp, yp, eval
    real(kind=8) :: dEl, dAz, kgEl, kgAz, zeta, norme
    common /bfup/ dEl, dAz, kgEl, kgAz, zeta, norme
!
    sigmax  = dsqrt( ( kgAz*dsin(zeta) )**2.d0 + ( kgEl*dcos(zeta) )**2.d0 )
    sigmay  = dsqrt( ( kgAz*dcos(zeta) )**2.d0 + ( kgEl*dsin(zeta) )**2.d0 )
    sigmaxy = ( kgEl**2.d0 - kgAz**2.d0 ) * dsin(zeta) * dcos(zeta)
    pi = 4.d0*datan(1.d0)
    pi2 = pi/2.d0
!
!   Mise en place de la périodicité de période pi
    xp = x
    eval = xp / pi2
    do while (dabs(eval).gt.1.d0)
      if (eval.gt. 1.d0) xp = xp - pi
      if (eval.lt.-1.d0) xp = xp + pi
      eval = xp / pi2
    end do
!
    yp = y
    eval = yp / pi2
    do while (dabs(eval).gt.1.d0)
      if (eval.gt. 1.d0) yp = yp - pi
      if (eval.lt.-1.d0) yp = yp + pi
      eval = yp / pi2
    end do
!
!   Calcul de la probabilité (attention dEl=dAz=0.d0 sinon la fonction ne norme pas)
    Xmu = xp
    Ymu = yp
    rho = sigmaxy/(sigmax*sigmay)
    z   = Xmu**2.d0/sigmax**2.d0 + Ymu**2.d0/sigmay**2.d0 - 2.d0*rho*Xmu*Ymu/(sigmax*sigmay)
    denom = 4.d0*pi*sigmax*sigmay*dsqrt(1.d0-rho**2.d0)
    p     = dexp( -z/(2.d0*(1.d0-rho**2.d0))) / denom * dcos(xp) ! cos(xp) est le jacobien de l'intégration dans le repère Azimut/Élévation
end subroutine bivariate_norme
