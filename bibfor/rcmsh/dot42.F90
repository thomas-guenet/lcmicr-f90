subroutine dot42(a, v, w)
! MODIF ALGORITH  DATE 18/11/2014   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "asterfort/r8inir.h"
    real(kind=8) :: a(6,6), v(6), w(6)
!
!   Produit d'une matrice (6,6) par un vecteur colonne (6,1)
!--------------------------------------------------------------
!   IN
!      a : matrice (6,6)
!      v : vecteur colonne (6,1)
!
!   OUT
!      w : vecteur colonne résultant (6,1)
!
!   VI
!      i, j : itérateurs
!--------------------------------------------------------------
    integer :: i, j
!
    call r8inir(6, 0.d0, w, 1)
    do 1 i = 1, 6
      do 2 j = 1, 6
        w(i) = w(i) + a(i,j)*v(j)
 2    end do
 1  end do
end subroutine dot42

