subroutine rtsafe(e, mat, x1, x2, xacc, rtsafed)
! MODIF ALGORITH  DATE 09/09/2015   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "rcmsh_headers/loading_func_deriv.h"
    real(kind=8) :: x1, x2, xacc, mat(13), e(3), rtsafed
!
!   Calcul de la fonction de chargement et de sa dérivée numérique
!--------------------------------------------------------------
!   IN
!      e         : Tenseur des déformations (totales)
!      mat       : Regroupement des données matériaux
!      x1        : Borne inférieur de la solution
!      x2        : Borne supérieur de la solution
!      xacc      : Amplitude du résidu à atteindre pour la convergence
!
!   OUT
!      rtsafed   : Valeur de la présente fonction
!
!   VI
!
!--------------------------------------------------------------
    integer :: MAXIT
    parameter (MAXIT=100)
    integer :: j
    real(kind=8) :: df, dx, dxold, f, fh, fl, temp, xh, xl
!
    call loading_func_deriv(mat, e, x1, fl, df)
    call loading_func_deriv(mat, e, x2, fh, df)
    if((fl.gt.0..and.fh.gt.0.).or.(fl.lt.0..and.fh.lt.0.)) then
       print*, 'rtsafed : roots not bracked (',x1,x2,');(',fl,fh,')'
    end if
    if (fl.eq.0.) then
      rtsafed=x1
      return
    else if (fh.eq.0.) then
      rtsafed=x2
      return
    else if (fl.lt.0.) then
      xl=x1
      xh=x2
    else
      xh=x1
      xl=x2
    end if
    rtsafed=.5d0*(x1+x2)
    dxold=dabs(x2-x1)
    dx=dxold
    call loading_func_deriv(mat, e, rtsafed, f, df)
    do 11 j=1,MAXIT
      if(((rtsafed-xh)*df-f)*((rtsafed-xl)*df-f).ge.0..or. abs(2.d0*&
           f).gt.dabs(dxold*df) ) then
        dxold=dx
        dx=0.5d0*(xh-xl)
        rtsafed=xl+dx
        if (xl.eq.rtsafed) return
      else
        dxold=dx
        dx=f/df
        temp=rtsafed
        rtsafed=rtsafed-dx
!
        if (temp.eq.rtsafed) return
      end if
      if (dabs(dx).lt.xacc) return
      call loading_func_deriv(mat, e, rtsafed, f, df)
      if (f.lt.0.) then
        xl=rtsafed
      else
        xh=rtsafed
      end if
11  continue
    print*, 'rtsafed exceeding maximum iterations'
    return
end subroutine rtsafe

