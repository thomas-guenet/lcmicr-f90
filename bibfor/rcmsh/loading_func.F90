subroutine loading_func(mat, eps, e, f)
! MODIF ALGORITH  DATE 08/10/2015   AUTEUR GUENET T.GUENET
! ======================================================================
! ======================================================================
    implicit none
#include "rcmsh_headers/Gaepsf.h"
#include "rcmsh_headers/Gbepsf.h"
#include "rcmsh_headers/Repsf.h"
    real(kind=8) :: mat(13), eps, e(3), f
!
!   Fonction de chargement / déchargement
!--------------------------------------------------------------
!   IN
!      mat : Regroupement des données matériaux
!      eps : Endommagement micromécanique
!      e   : Tenseur des déformations propres (3)
!
!   OUT
!      f : Valeur de la fonction de chargement/déchargement
!
!   VI
!      Gaeps : Taux de dissipation d'énergie de la matrice [MPa]
!      Gbeps : Taux de dissipation d'énergie des fibres [MPa]
!      Reps  : [MPa]
!--------------------------------------------------------------
    real(kind=8) :: Gaeps, Gbeps, Reps
!
!   Gbeps en premier au cas ou eps est modifié par Gbeps
    if (e(3).lt.0.d0) then ! si matériau en compression alors pas de dissipation d'énergie
        Gbeps = 0.d0       ! cela permet d'éviter un floating point error dans le calcul de dWda
    else
        call Gbepsf(mat, eps, e, Gbeps) 
    end if
    call Gaepsf(mat, eps, e, Gaeps)
    call Repsf (mat, eps, Reps)
    !f = Gaeps + Gbeps - 2.d0*dsqrt(Gaeps*Gbeps) - Reps - 1.d-4 !!!!! 2.d0*dsqrt  1d-4 = oscillation autour de la solution f(epsmi)
    f = Gaeps - Gbeps - Reps ! simplification
end subroutine loading_func

