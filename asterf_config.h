! WARNING! Automatically generated by `waf configure`!


#ifndef ASTERF_CONFIG_H
#define ASTERF_CONFIG_H

#define _USE_OPENMP 1
#define PYTHONDIR "/home/thomas/dev/codeaster/install/std/lib/python2.7/site-packages"
#define PYTHONARCHDIR "/home/thomas/dev/codeaster/install/std/lib/python2.7/site-packages"
#define HAVE_PYTHON_H 1
#define _USE_64_BITS 1
#define _POSIX 1
! #undef _WINDOWS
#define LINUX64 1
#define HAVE_HDF5_H 1
#define HAVE_HDF5 1
#define HAVE_MED_H 1
#define MED_INT_SIZE 4
#define _USE_MED_SHORT_INT 1
#define HAVE_MED 1
#define HAVE_METIS_H 1
#define _HAVE_METIS 1
#define HAVE_METIS 1
#define ASTER_MUMPS_VERSION "4.10.0"
#define MUMPS_INT_SIZE 4
#define _HAVE_MUMPS 1
#define HAVE_MUMPS 1
#define HAVE_STDIO_H 1
#define HAVE_SCOTCH 1
! #undef _DISABLE_SCOTCH
#define _DISABLE_PETSC 1
! #undef HAVE_PETSC
! #undef _NO_UNDERSCORE
#define HAVE_ASTER_TYPES 1
#define ASTER_INT4_SIZE 4
#define ASTER_INT_SIZE 8
#define ASTER_LOGICAL_SIZE 1
#define ASTER_REAL4_SIZE 4
#define ASTER_REAL8_SIZE 8
#define ASTER_COMPLEX_SIZE 16
#define BLAS_INT_SIZE 4
! #undef STRINGIFY_USE_OPERATOR
#define STRINGIFY_USE_QUOTES 1
#define HAVE_BACKTRACE 1
! #undef HAVE_TRACEBACKQQ
#define _DISABLE_MFRONT 1
! #undef HAVE_MFRONT
#define ASTERBEHAVIOUR "AsterBehaviour"

#endif
