#!/bin/bash
make mrproper
cp /home/thomas/dev/lcmicr-f90/bibfor/algorith/redecetg.F90 /home/thomas/dev/codeaster/src/bibfor/algorith/
cp /home/thomas/dev/lcmicr-f90/bibfor/include/asterfort/*tg.h /home/thomas/dev/codeaster/src/bibfor/include/asterfort/
cp -r /home/thomas/dev/lcmicr-f90/bibfor/include/rcmsh_headers /home/thomas/dev/codeaster/src/bibfor/include/
cp /home/thomas/dev/lcmicr-f90/bibfor/lc/*.F90 /home/thomas/dev/codeaster/src/bibfor/lc/
cp /home/thomas/dev/lcmicr-f90/bibfor/nonlinear/*.F90 /home/thomas/dev/codeaster/src/bibfor/nonlinear/
cp -r /home/thomas/dev/lcmicr-f90/bibfor/rcmsh /home/thomas/dev/codeaster/src/bibfor/
cp /home/thomas/dev/lcmicr-f90/bibpyt/Comportement/*.py /home/thomas/dev/codeaster/src/bibpyt/Comportement/
cp /home/thomas/dev/lcmicr-f90/catapy/commande/defi_materiau.capy /home/thomas/dev/codeaster/src/catapy/commande/
cp /home/thomas/dev/lcmicr-f90/catapy/commun/c_relation.capy      /home/thomas/dev/codeaster/src/catapy/commun/

echo "Ne pas oublier de :"
echo "   - désactiver les variables inutilisées (dummy)"
echo "   - réactiver les variables matériau et rcvalb"
echo "   - problèmes lié à L0 : réactiver dfdm3d"
