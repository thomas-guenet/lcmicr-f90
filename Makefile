CC=gfortran
CFLAGS=-Wall -g -I bibfor/include/
LDFLAGS=
EXEC=rcmsh.exe
SRC= $(wildcard *.F90 \
	bibfor/algeline/*.F90 \
	bibfor/algorith/bptobg.F90 \
	bibfor/algorith/matini.F90 \
	bibfor/rcmsh/*.F90 \
	bibfor/utilifor/*.F90 )
#	bibfor/nonlinear/*.F90 \ # nmpl3d n'est pas appeler par main, il ne faut donc pas l'appeler
#	bibfor/lc/lc0068.F90 \   # lc0068 n'ets pas appeler par main, ce qui permet d'évité l'intégration de bcp de .h
OBJ= $(SRC:.F90=.o)
DEP= $(wildcard *.h \
		bibfor/include/*.h \
		bibfor/include/rcmsh_headers/*.h \
		bibfor/include/asterfort/*.h )

# Tous les exécutables qu’il faut construire
#==============================================
all: $(EXEC)


# Règle de dépendances :
#========================
rcmsh.exe: $(OBJ)
	@$(CC) -o $@ $^ $(LDFLAGS)
main.o: $(DEP)

# Règle d’inférence :
#=====================
%.o: %.F90 $(DEP) #tous les fichiers fortran sont dépendants des headers 
	@$(CC) -o $@ -c $< $(CFLAGS) 
# $< car pas besoin d'y inclure les headers (ils sont inclus grace à gfortran)


# Règle de nettoyage :
#======================
.PHONY: clean mrproper

clean:
	#rm -rf *.o
	find . -name '*.o' -print0 | xargs -0 rm

mrproper: clean
	rm -rf $(EXEC)
	rm ddd*
	find . -name '*~' -print0 | xargs -0 rm 


